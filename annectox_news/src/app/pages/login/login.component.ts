import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AlertController, LoadingController  } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  loading;
  registerCredentials = {email: '', password: ''};

  constructor(
    private loadingController: LoadingController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  public createAccount() {
   // this.nav.push(RegisterPage, null, {animation:"md-transition", direction: 'left'});
  }

  public login() {
    this.showLoading();
    // this.auth.callApi('get', '/auth', {UserName: this.registerCredentials.email, Password: this.registerCredentials.password})
    // .subscribe((user)=>{
    //   localStorage.setItem('token', user.data.JWT);
    //   localStorage.setItem('username', this.registerCredentials.email);
    //   localStorage.setItem('password', this.registerCredentials.password);
    //   this.loading.dismiss();
    //   this.nav.setRoot(TabsPage)
    // }, this.showError.bind(this))

  }

  async  showLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    });
    await this.loading.present();
  }

  signThrowTwitter() {
    // this.showLoading();
    // this.twitterApi.login().then(
    //   resp=>{
    //     this.twitterApi.getUserProfile().then(
    //       user=>{
    //
    //       },
    //       err=>{
    //         this.showError.bind(this);
    //       }
    //     )
    //   },
    //   this.showError.bind(this)
    // );
    // this.showLoading();
    // this.twitterApi.login().then(resp=>{
    //     this.loading.dismiss();
    //     alert(JSON.stringify(resp));
    //     this.nav.setRoot(TabsPage)
    // }, this.showError.bind(this));
  }

  signThrowLinkedIn() {
    // this.showLoading();
    // this.oauth.logInVia(this.linkedinProvider).then((response) => {
    //     let req = {
    //       'grant_type': 'authorization_code',
    //       code: response['code'],
    //       redirect_uri: 'http://192.168.1.38:8100',
    //       client_id: '86hdh92rfe0lnh',
    //       client_secret: 'wakuBYwJe4rvTd0q'
    //     };
    //     console.log(this.constants)
    //     this.auth.callApi('post', this.constants.linkedinOauth+'', req, {'Content-type': 'application/x-www-form-urlencoded'})
    //     .subscribe((linkedinToken)=>{
    //       this.auth.callApi('get', this.constants.linkedin+'/people/~?oauth2_access_token='+linkedinToken.access_token+'&format=json')
    //       .subscribe((user)=>{
    //         this.loading.dismiss();
    //         localStorage.setItem('linkedin-token', linkedinToken.access_token);
    //         this.nav.setRoot(TabsPage)
    //       }, this.showError.bind(this))
    //     })
    //   }, this.showError.bind(this));
  }

  forgotPassword(){

  }

  signThrowFacebook(){
    // this.showLoading();
    // this.facebookApi.login().then((response) => {
    //   this.facebookApi.getFullUserProfile().then((response) => {
    //     this.loading.dismiss();
    //     this.nav.setRoot(TabsPage);
    //   }, (error)=>{
    //     this.showError.call(this, error.errorMessage);
    //   });
    // }, (error)=>{
    //   this.showError.call(this, error.errorMessage);
    // })
  }

  async  showError(text) {
    this.loading.dismiss();
    const alert = await  this.alertController.create({
      header: 'Alert',
      message: text,
      buttons: ['OK']
    });
    await alert.present();
  }
}
